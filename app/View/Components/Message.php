<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Message extends Component
{
    /**
     * @var string
     */
    public $message;

    /**
     * Create a new component instance.
     *
     * @param string $message
     */
    public function __construct($message='')
    {
        $this->message = $message;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        if($this->message !== '')
            return view('components.message');
    }
}
