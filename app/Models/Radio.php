<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Radio extends Base
{
    protected $fillable = [
        'name',
        'category_id',
        'epg_id',
        'stream_logo',
        'stream_link',
        'status',
        'notes',
    ];

    public static $cols = ["uuid", "name"];

    public function bouquets() {
        return $this->hasMany(BouquetRelation::class, 'resource_id', 'uuid');
    }

    public function available_bouquets() {
        return $this->bouquets()->where('resource', '=', 'radio');
    }
}
