<script>

    axios.defaults.headers.common = {
        'X-Requested-With': 'XMLHttpRequest',
        'X-CSRF-TOKEN' : document.querySelector('meta[name="csrf-token"]').getAttribute('content')
    };

    function setPassword(id) {

        Swal.fire({
            title: 'Insira a nova senha',
            input: 'text',
            inputAttributes: {
                autocapitalize: 'off'
            },
            showCancelButton: true,
            confirmButtonText: 'Salvar',
            showLoaderOnConfirm: true,
            preConfirm: (password) => {
                return axios.post('/password/admins', {
                    password,
                    uuid: id
                }).catch(function (error) {
                    Swal.showValidationMessage(
                        "Senha é um parametro obrigatório"
                    )
                })
            },
            allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
            if (result.value) {
                Swal.fire({
                    title: `Senha Alterada`,
                })
            }
        })
    }

</script>
