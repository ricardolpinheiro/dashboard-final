<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminRequest;
use App\Jobs\TmdbFetchSerie;
use App\Models\Bouquet;
use App\Models\BouquetRelation;
use App\Models\TvSerie as Resource;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class SerieController extends Controller
{
    private $res = 'series';

    private $bouquets;

    public function __construct()
    {

        $bouquets = Bouquet::where('status', 1)->get();
        $bouquets = $bouquets->toArray();

        $this->bouquets =  parseSelectArray($bouquets, 'name');;
    }

    public function index()
    {
        $res = $this->res;
        return view('admin.'.$this->res.'.index')->with(compact('res'));
    }

    public function datatable(Request $request) {

        $model = Resource::select(Resource::$cols)->get();
        return Datatables::of($model)
            ->addColumn('action', function ($model) {
                $btn = btnTableEdit($this->res.'.edit', $model);
                $btn .= btnTableDelete($this->res.'.destroy', $model);
                return $btn;
            })
            ->addColumn('poster', function ($model) {
                return imgOnTable($model->poster_path);
            })
            ->addColumn('background', function ($model) {
                return imgOnTable($model->backdrop_path);
            })
            ->addColumn('bouquets', function ($model) {
                $m = Resource::where('uuid', '=', $model->uuid)->first();
                return bouquetOnTable($m, 'serie');
            })
            ->rawColumns(['action', 'poster', 'background', 'bouquets'])
            ->make(true);
    }

    public function create()
    {
        $res = $this->res;
        $bouquets = $this->bouquets;
        return view('admin.'.$this->res.'.create')->with(compact('res', 'bouquets'));
    }

    public function store(Request $request)
    {
        $res = $this->res;
        $input = $request->all();

        $request->validate([
            'tmdb_id' => 'required_without:title',
            'title' => 'required_without:tmdb_id',
            'poster_path' => 'mimes:jpeg,bmp,png,jpg',
            'backdrop_path' => 'mimes:jpeg,bmp,png,jpg',
        ]);

        if($request->hasFile('poster_path'))
            $input['poster_path'] = storage($request, 'poster_path', '/img/stream/');

        if($request->hasFile('stream_logo'))
            $input['backdrop_path'] = storage($request, 'backdrop_path', '/img/stream/');

        $model = Resource::create($input);

        dispatch(new TmdbFetchSerie($model));

        if($model && isset($input['bouquet_id'])) {
            foreach ($input['bouquet_id'] as $bouquet){
                BouquetRelation::create(['resource_id' => $model->uuid, 'resource' => 'serie', 'bouquet_id'=>$bouquet]);
            }
        }

        return redirect(route($this->res.'.index'))->with('success', __('messages.'.$this->res.'.created'));
    }

    public function show($id)
    {
        $res = $this->res;
        $model = Resource::where('uuid', '=', $id)->first();

        if (empty($model)) {
            return redirect(route($this->res.'.index'))->with('error', __('messages.'.$this->res.'.not_found'));
        }

        return view('admin.'.$this->res.'.show')->with(compact('model'));
    }

    public function edit($id)
    {
        $res = $this->res;
        $bouquets = $this->bouquets;
        $model = Resource::where('uuid', '=', $id)->first();

        if (empty($model)) {
            return redirect(route($this->res.'.index'))->with('error', __('messages.'.$this->res.'.not_found'));
        }

        return view('admin.'.$this->res.'.edit')->with(compact('model', 'res', 'bouquets'));
    }

    public function update($id, Request $request)
    {
        $input = $request->all();
        $models = Resource::where('uuid', '=', $id)->first();
        $dispatch = false;

        if (empty($models)) {
            return redirect(route($this->res.'.index'))->with('error', __('messages.'.$this->res.'.not_found'));
        }

        if($input['tmdb_id'] != $models->tmdb_id || !empty($models->tmdb_id) || !is_null($models->tmdb_id)) {
            $dispatch = true;
        }

        $validate = [
            'title' => 'required',
            'detail' => 'required',
            'poster_path' => 'required|mimes:jpeg,bmp,png,jpg',
            'backdrop_path' => 'required|mimes:jpeg,bmp,png,jpg',
        ];


        if(empty($request->get('poster_path'))){
            unset($validate['poster_path']);
        }

        if(empty($request->get('backdrop_path'))){
            unset($validate['backdrop_path']);
        }

        $request->validate($validate);

        if($request->hasFile('poster_path'))
            $input['poster_path'] = storage($request, 'poster_path', '/img/stream/');

        if($request->hasFile('stream_logo'))
            $input['backdrop_path'] = storage($request, 'backdrop_path', '/img/stream/');


        $models->fill($input);
        $models->save();

        if($models){
            $relation = BouquetRelation::where(['resource_id' => $models->uuid, 'resource' => 'serie']);
            if($relation->count()) {
                $relation->delete();
            }

            if(isset($input['bouquet_id'] )) {
                foreach ($input['bouquet_id'] as $bouquet){
                    BouquetRelation::create(['resource_id' => $models->uuid, 'resource' => 'serie', 'bouquet_id'=>$bouquet]);
                }
            }

        }
        //if($dispatch) dispatch(new SyncTMDBSerie($serie));

        return redirect(route($this->res.'.index'))->with('success', __('messages.'.$this->res.'.update'));
    }


    public function destroy($id)
    {
        $models = Resource::where('uuid', '=', $id)->first();

        if (empty($models)) {
            return redirect(route($this->res.'.index'))->with('error', __('messages.'.$this->res.'.not_found'));
        }

        $models->delete();

        return redirect(route($this->res.'.index'));
    }
}
