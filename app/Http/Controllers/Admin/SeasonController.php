<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminRequest;
use App\Jobs\TmdbFetchSeason;
use App\Jobs\TmdbFetchSerie;
use App\Models\Bouquet;
use App\Models\BouquetRelation;
use App\Models\TvSerie;
use App\Models\TvSerieSeason as Resource;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class SeasonController extends Controller
{
    private $res = 'seasons';

    private $bouquets;
    public $series;

    public function __construct()
    {
        $series = TvSerie::all();
        $series = $series->toArray();

        $bouquets = Bouquet::where('status', 1)->get();
        $bouquets = $bouquets->toArray();

        $this->bouquets =  parseSelectArray($bouquets, 'name');
        $this->series =  parseSelectArray($series, 'title');
    }

    public function index()
    {
        $res = $this->res;
        return view('admin.'.$this->res.'.index')->with(compact('res'));
    }

    public function datatable(Request $request) {

        $model = Resource::select(Resource::$cols)->get();
        return Datatables::of($model)
            ->addColumn('action', function ($model) {
                $btn = btnTableEdit($this->res.'.edit', $model);
                $btn .= btnTableDelete($this->res.'.destroy', $model);
                return $btn;
            })
            ->editColumn("season_no", 'Temporada {{$season_no}}')
            ->addColumn('serie', function ($model) {
                $m = Resource::where('uuid', '=', $model->uuid)->first();
                return $m->serie->title;
            })
            ->addColumn('poster', function ($model) {
                return imgOnTable($model->poster_path);
            })
            ->rawColumns(['action', 'poster'])
            ->make(true);
    }

    public function create()
    {
        $res = $this->res;
        $series = $this->series;
        return view('admin.'.$this->res.'.create')->with(compact('res', 'series'));
    }

    public function store(Request $request)
    {
        $res = $this->res;
        $input = $request->all();

        $request->validate([
            'tv_series_id' => 'required|exists:tv_series,uuid',
            'poster_path' => 'mimes:jpeg,bmp,png,jpg',
        ]);

        $model = Resource::create($input);

        if($request->hasFile('poster_path'))
            $input['poster_path'] = storage($request, 'poster_path', '/img/stream/');

        dispatch(new TmdbFetchSeason($model));

        return redirect(route($this->res.'.index'))->with('success', __('messages.'.$this->res.'.created'));
    }

    public function show($id)
    {
        $res = $this->res;
        $model = Resource::where('uuid', '=', $id)->first();

        if (empty($model)) {
            return redirect(route($this->res.'.index'))->with('error', __('messages.'.$this->res.'.not_found'));
        }

        return view('admin.'.$this->res.'.show')->with(compact('model'));
    }

    public function edit($id)
    {
        $res = $this->res;
        $bouquets = $this->bouquets;
        $series = $this->series;
        $model = Resource::where('uuid', '=', $id)->first();

        if (empty($model)) {
            return redirect(route($this->res.'.index'))->with('error', __('messages.'.$this->res.'.not_found'));
        }

        return view('admin.'.$this->res.'.edit')->with(compact('model', 'res', 'bouquets', 'series'));
    }

    public function update($id, Request $request)
    {
        $input = $request->all();
        $models = Resource::where('uuid', '=', $id)->first();

        if (empty($models)) {
            return redirect(route($this->res.'.index'))->with('error', __('messages.'.$this->res.'.not_found'));
        }

        $validate = [
            'tv_series_id' => 'required|exists:tv_series,uuid',
            'poster_path' => 'mimes:jpeg,bmp,png,jpg',
        ];

        if(empty($request->get('poster_path'))){
            unset($validate['poster_path']);
        }

        if(empty($request->get('backdrop_path'))){
            unset($validate['backdrop_path']);
        }

        $request->validate($validate);

        if($request->hasFile('poster_path'))
            $input['poster_path'] = storage($request, 'poster_path', '/img/stream/');

        if($request->hasFile('stream_logo'))
            $input['backdrop_path'] = storage($request, 'backdrop_path', '/img/stream/');

        $models->fill($input);
        $models->save();

        if($models){
            $relation = BouquetRelation::where(['resource_id' => $models->uuid, 'resource' => 'serie']);
            if($relation->count()) {
                $relation->delete();
            }

            if(isset($input['bouquet_id'] )) {
                foreach ($input['bouquet_id'] as $bouquet){
                    BouquetRelation::create(['resource_id' => $models->uuid, 'resource' => 'serie', 'bouquet_id'=>$bouquet]);
                }
            }

        }

        return redirect(route($this->res.'.index'))->with('success', __('messages.'.$this->res.'.update'));
    }


    public function destroy($id)
    {
        $models = Resource::where('uuid', '=', $id)->first();

        if (empty($models)) {
            return redirect(route($this->res.'.index'))->with('error', __('messages.'.$this->res.'.not_found'));
        }

        $models->delete();

        return redirect(route($this->res.'.index'));
    }
}
