<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bouquet extends Base
{
    protected $fillable = ["name", "status"];

    public static $cols = ['uuid', 'name', 'status'];
}
