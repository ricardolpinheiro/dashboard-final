<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StreamStatus extends Base
{

    protected $dates = ['last_check_uptime'];

    protected $fillable = [
        'uuid',
        'stream_id',
        'link_id',
        'uptime',
        'donwtime',
        'status',
        'last_check_uptime',
    ];

}
