@extends('layouts.app')

@section('content')

    <div class="element-wrapper">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <h6 class="element-header">
                        Cadastro de {{ getTitlePage($res) }}
                    </h6>
                </div>
                <div class="col-md-6 text-right">
                    <button onclick="window.history.back();"  class="mr-2 mb-2 btn btn-primary btn-md" type="button">Voltar</button>
                </div>
            </div>
        </div>
        {!! Form::open(['route' => $res.'.store', 'enctype' => 'multipart/form-data']) !!}
            <div class="row">
                <div class="col-md-6">
                    <div class="element-box">
                        @include('admin.'.$res.'.fields')
                        <div class="form-group col-sm-12">
                            {!! Form::submit('Salvar', ['class' => 'btn btn-primary']) !!}
                        </div>
                    </div>
                </div>
           </div>
        {!! Form::close() !!}
    </div>
@endsection
