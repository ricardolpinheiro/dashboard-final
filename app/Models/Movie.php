<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Movie extends Base
{
    protected $appends = ['year'];

    protected $fillable = [
        "tmdb_id",
        "title",
        "category_id",
        "movie_link",
        "adult",
        "duration",
        "thumbnail",
        "poster",
        "collection_id",
        "trailer_url",
        "detail",
        "status",
        "home_screen",
        "is_home_screen",
        "rating",
        "released",
        "type",
    ];

    public static $cols = ["uuid", "title"];

//    public function collection() {
//        return $this->hasOne(Collection::class, 'id', 'collection_id');
//    }
//
//    public function genres() {
//        return $this->hasMany(GenreMovie::class, 'movie_id', 'id');
//    }

    public function getYearAttribute() {
        if(isset($this->attributes['released'])) {
            $explode = explode("-", $this->attributes['released']);
            return  $explode[0] ?? 2019;
        }
        return '';
    }

    public function bouquets() {
        return $this->hasMany(BouquetRelation::class, 'resource_id', 'uuid');
    }

    public function available_bouquets() {
        return $this->bouquets()->where('resource', '=', 'movie');
    }
}
