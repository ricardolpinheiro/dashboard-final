<x-input name="name" text="Nome" :value="$model->name ?? ''" placeholder="Digite o Nome da Stream" :error="$errors"/>

<x-input-prepend prepend="http://" name="stream_link" text="Stream URL" :value="$model->stream_link ?? ''" placeholder="Insira a URL da Stream" :error="$errors"/>

<x-select2 name="category_id" text="Categoria" :value="$model->category_id ?? ''" placeholder="Selecione a Categoria" :error="$errors"
           :options="$categories"/>

<x-multiselect name="bouquet_id[]" text="Bouquets" :value="$model->available_bouquets ?? ''" placeholder="Selecione o(s) Bouquet(s)" :error="$errors"
               :options="$bouquets"/>

<x-input-file name="stream_logo" text="Stream Logo" :value="$model->stream_logo ?? ''" placeholder="" :error="$errors"/>

<x-text-area name="notes" text="Observação" :value="$model->notes ?? ''" placeholder="Digite o texto" :error="$errors"/>
