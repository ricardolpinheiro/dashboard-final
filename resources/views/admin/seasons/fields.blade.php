@if(isset($model))
    <x-input type="number" name="season_no" text="Season" :value="$model->season_no ?? ''" placeholder="Digite a Season" :error="$errors"/>
@else
    <x-input disabled="true" type="number" name="season_no" text="Season" :value="$model->season_no ?? ''" placeholder="Digite a Season" :error="$errors"/>
@endif

<x-select2 name="tv_series_id" text="Série" placeholder="Selecione a Série de TV" :error="$errors"
           :options="$series"/>

<x-text-area name="detail" text="Overview" :value="$model->detail ?? ''" placeholder="Digite o texto" :error="$errors"/>

@if(isset($model))
    <div class="profile-tile">
        <a class="profile-tile-box" href="{{ $model->poster_path ?? '' }}" target="_blank">
            <div class="pt-avatar-w">
                <img alt="" src="{{ $model->poster_path ?? ''}}">
            </div>
        </a>
        <div class="pt-btn">
            <button type="button" class="btn btn-secondary btn-sm" onclick="$('#poster_path').trigger('click');">Alterar</button>
        </div>
    </div>
    <x-input-file display="none" id="poster_path" name="poster_path" text="Poster" :value="$model->poster_path ?? ''" placeholder="" :error="$errors"/>
@else
    <x-input-file name="poster_path" text="Poster" placeholder="" :error="$errors"/>

@endif
