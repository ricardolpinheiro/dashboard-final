<div class="form-group {{ ($hasError == true) ? "has-error has-danger" : '' }}">
    <label for="{{ $name }}">{{ __(ucfirst($text ?? $name)) }}</label>
    <textarea class="form-control" rows="5" name="{{ $name }}" placeholder="{{ $placeholder }}" > {{ $value }}</textarea>
</div>
