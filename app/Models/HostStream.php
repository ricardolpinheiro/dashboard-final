<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HostStream extends Base
{

    protected $fillable = ['host_name'];

    public static $cols = ['uuid', 'host_name'];

}
