<?php

namespace App\Jobs;

use App\Models\Genre;
use App\Models\GenreSerie;
use App\Models\TvSerie;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\URL;

class TmdbFetchSerie implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var TvSerie
     */
    private $serie;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(TvSerie $serie)
    {
        $this->serie = $serie;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if(empty($this->serie->tmdb_id)) {
            $this->syncByName();
        } else {
            $this->syncById();
        }
    }

    function syncByName() {
        $serie = tmdbGetSerieByName($this->serie->title);
        if($serie->total_results > 0) {
            $this->serie->title = $serie->results[0]->name;
            $this->serie->tmdb_id = $serie->results[0]->id;
            $this->serie->original_name = $serie->results[0]->original_name;
            $this->serie->save();
            $this->syncById();
        }
    }

    function syncById() {
        $serie = tmdbGetSerieById($this->serie->tmdb_id);
        if(isset($serie) && isset($serie->id)) {
            $this->serie->title = $serie->name;
            $this->serie->original_name = $serie->original_name;
            $this->serie->poster_path = empty($serie->poster_path) ? $serie->backdrop_path : $serie->poster_path;
            $this->serie->backdrop_path = empty($serie->backdrop_path) ? $serie->poster_path : $serie->backdrop_path;
            $this->serie->detail = $serie->overview;
            $this->serie->rating = $serie->vote_average;
            $this->serie->save();
            $this->assertGenres($serie);
            dispatch(new FetchImage('serie',  $this->serie));
        }
    }

    public function assertGenres($serie) {
        if(isset($serie->genres)) {
            foreach ($serie->genres as $serie) {

                $find = Genre::where('tmdb_id', '=', $serie->id)->get();
                if(!$find->count()) {
                    $g['name'] = $serie->name;
                    $g['tmdb_id'] = $serie->id;
                    $find = Genre::create($g);
                    GenreSerie::create(['serie_id' => $this->serie->uuid, 'genre_id' => $find->uuid]);
                } else {
                    $find =  $find[0];
                    $gs = GenreSerie::where('serie_id', '=',  $this->serie->uuid)->where('genre_id', '=', $find->uuid)->get();
                    if(!$gs->count() > 0) {
                        GenreSerie::create(['serie_id' => $this->serie->uuid, 'genre_id' => $find->uuid]);
                    }
                }
            }
        }
    }
}
