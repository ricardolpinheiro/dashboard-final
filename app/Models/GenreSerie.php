<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GenreSerie extends Base
{
    protected $fillable = [
        "serie_id",
        "genre_id",
        "tmdb_id",
    ];
}
