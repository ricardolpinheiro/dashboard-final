<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminRequest;
use App\Models\Bouquet;
use App\Models\Revenda as Resource;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class RevendaController extends Controller
{
    private $res = 'revendas';

    private $bouquets;

    public function __construct()
    {
        $bouquets = Bouquet::where('status', 1)->get();
        $bouquets = $bouquets->toArray();

        $this->bouquets =  parseSelectArray($bouquets, 'name');;
    }

    public function index()
    {
        $res = $this->res;
        return view('admin.'.$this->res.'.index')->with(compact('res'));
    }

    public function datatable(Request $request) {

        $model = Resource::select(Resource::$cols)->get();

        return Datatables::of($model)
            ->addColumn('action', function ($model) {
                $btn = btnTableEdit($this->res.'.edit', $model);
                $btn .= btnTablePassword($model);
                $btn .= btnTableDelete($this->res.'.destroy', $model);
                return $btn;
            })
            ->make(true);
    }

    public function create()
    {
        $res = $this->res;
        $bouquets = $this->bouquets;
        return view('admin.'.$this->res.'.create')->with(compact('res', 'bouquets'));
    }

    public function store(AdminRequest $request)
    {
        $res = $this->res;
        $input = $request->all();
        $model = Resource::create($input);
        return redirect(route($this->res.'.index'))->with('success', __('messages.'.$this->res.'.created'));
    }

    public function show($id)
    {
        $res = $this->res;
        $model = Resource::where('uuid', '=', $id)->first();

        if (empty($model)) {
            return redirect(route($this->res.'.index'))->with('error', __('messages.'.$this->res.'.not_found'));
        }

        return view('admin.'.$this->res.'.show')->with(compact('model'));
    }

    public function edit($id)
    {
        $res = $this->res;
        $model = Resource::where('uuid', '=', $id)->first();

        if (empty($model)) {
            return redirect(route($this->res.'.index'))->with('error', __('messages.'.$this->res.'.not_found'));
        }

        return view('admin.'.$this->res.'.edit')->with(compact('model', 'res'));
    }

    public function update($id, Request $request)
    {
        $models = Resource::where('uuid', '=', $id)->first();

        if (empty($models)) {
            return redirect(route($this->res.'.index'))->with('error', __('messages.'.$this->res.'.not_found'));
        }

        $models->fill($request->all());
        $models->save();

        return redirect(route($this->res.'.index'))->with('success', __('messages.'.$this->res.'.update'));
    }


    public function destroy($id)
    {
        $models = Resource::where('uuid', '=', $id)->first();

        if (empty($models)) {
            return redirect(route($this->res.'.index'))->with('error', __('messages.'.$this->res.'.not_found'));
        }

        $models->delete();

        return redirect(route($this->res.'.index'));
    }
}
