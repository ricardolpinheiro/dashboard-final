<?php

namespace App\Jobs;

use Gumlet\ImageResize;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Ixudra\Curl\Facades\Curl;

class FetchImage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $res;
    private $model;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($res, $model)
    {
        $this->res = $res;
        $this->model = $model;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        switch ($this->res) {
            case 'movie':
                $this->getResourceMovie();
                break;
            case 'serie':
                $this->getResourceSerie();
                break;
            case 'season':
                $this->getResourceSeason();
                break;
            case 'episode':
                $this->getResourceEpisode();
                break;
            case 'collection';
                $this->getResourceCollection();
                break;
            default:
                return;
        }
    }


    function getResourceEpisode() {
        if(!empty($this->model->thumbnail)) {
            $url = URL_RESOURCE_ORIRINAL . $this->model->thumbnail;
            if(checkExistsUrl($url)) {
                $image = storage_resource($url, PATH_RESOURCE_SERIES, 800);
                $this->model->thumbnail = $image;
                $this->model->save();
            }

        }
    }

    public function getResourceSerie(){

        if(!empty($this->model->poster_path)) {
            $url = URL_RESOURCE_ORIRINAL . $this->model->poster_path;
            if(checkExistsUrl($url)) {
                $image = storage_resource($url, PATH_RESOURCE_SERIES);
                $this->model->poster_path = $image;
                $this->model->save();
            }
        }

        if(!empty($this->model->backdrop_path)) {
            $url = URL_RESOURCE_ORIRINAL . $this->model->backdrop_path;
            if(checkExistsUrl($url)) {
                $image = storage_resource($url, PATH_RESOURCE_SERIES, 800);
                $this->model->backdrop_path = $image;
                $this->model->save();
            }
        }
    }

    public function getResourceSeason(){

        if(!empty($this->model->poster_path)) {
            $url = "http://image.tmdb.org/t/p/original" . $this->model->poster_path;
            if(checkExistsUrl($url)) {
                $image = storage_resource($url, PATH_RESOURCE_SERIES);

                $this->model->poster_path = $image;
                $this->model->save();
            }
        }
    }

    public function getResourceMovie(){
        if(!empty($this->model->thumbnail && $this->model->thumbnail != '')) {
            $url = "http://image.tmdb.org/t/p/original" . $this->model->thumbnail;
            if(checkExistsUrl($url)) {
                $image = storage_resource($url, PATH_RESOURCE_MOVIES);

                $this->model->thumbnail = $image;
                $this->model->save();
            }

        }

        if(!empty($this->model->poster && $this->model->poster != '')) {
            $url = "http://image.tmdb.org/t/p/w500" . $this->model->poster;
            if(checkExistsUrl($url)) {
                $image = storage_resource($url, PATH_RESOURCE_MOVIES);

                $this->model->poster = $image;
                $this->model->save();
            }

        }
    }

    public function getResourceCollection(){


        if(!empty($this->model->poster_path)) {
            $url = "http://image.tmdb.org/t/p/original" . $this->model->poster_path;
            if(checkExistsUrl($url)) {
                $image = storage_resource($url, PATH_RESOURCE_SERIES);

                $this->model->poster_path = $image;
                $this->model->save();
            }

        }

        if(!empty($this->model->backdrop_path) && $this->model->backdrop_path != '') {
            $url = "http://image.tmdb.org/t/p/w500" . $this->model->backdrop_path;
            if(checkExistsUrl($url)) {
                $image = storage_resource($url, PATH_RESOURCE_SERIES);

                $this->model->backdrop_path = $image;
                $this->model->save();
            }

        }

    }
}
