<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GenreMovie extends Base
{
    protected $fillable = [
        "movie_id",
        "genre_id",
        "tmdb_id",
    ];

    public function genre() {
        return $this->hasOne(Genre::class, 'uuid', 'genre_id');
    }
}
