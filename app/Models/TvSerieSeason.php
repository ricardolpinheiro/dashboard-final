<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class TvSerieSeason extends Base
{

    public static $cols = ['uuid', 'season_no', 'poster_path'];

    protected $fillable = [
        "tv_series_id",
        "tmdb_id",
        "season_no",
        "poster_path",
        "detail",
        "type",
        "deleted_by",
    ];

//    public function episodes() {
//        return $this->hasMany(TvSerieSeasonEpisode::class, 'seasons_id', 'id');
//    }

    public function episodes() {
        return $this->hasMany(TvSerieEpisode::class, 'seasons_id', 'uuid');
    }

    public function serie() {
        return $this->hasOne(TvSerie::class, 'uuid', 'tv_series_id');
    }

}
