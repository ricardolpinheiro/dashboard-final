<x-input name="name" text="Nome" :value="$model->name ??  ''" placeholder="Digite o Nome" :error="$errors"/>

<x-select2 name="status" text="Status" placeholder="Selecione o Status" :error="$errors"
           :options="array('1' => 'Ativo', '0' =>'Inativo')"/>
