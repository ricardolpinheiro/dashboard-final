<?php

namespace App\Providers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        DB::listen(function($query) {
            logger()->info($query->sql . print_r($query->bindings, true));
        });

        ini_set('max_execution_time ',300);
        define('TMDB_API_KEY',  env('TMDB_API_KEY'));

        define('REPLACEMENT_ID',  '{#ID}');
        define('REPLACEMENT_NAME',  '{#NAME}');

        define('REPLACEMENT_SEASON',  '{#NAME}');
        define('REPLACEMENT_EPISODE',  '{#EPISODE}');

        //FILMES
        define('TMDB_API_FIND_BY_ID', "https://api.themoviedb.org/3/movie/".REPLACEMENT_ID."?language=pt-BR&api_key=".TMDB_API_KEY);
        define('TMDB_API_FIND_BY_NAME', "https://api.themoviedb.org/3/search/movie?language=pt-BR&api_key=".TMDB_API_KEY."&query=".REPLACEMENT_NAME);

        //COLLECTION
        define('TMDB_API_FIND_COLLECTION', "https://api.themoviedb.org/3/collection/".REPLACEMENT_ID."?language=pt-BR&api_key=".TMDB_API_KEY);

        //SERIES
        define('TMDB_API_FIND_SERIE_BY_NAME', "https://api.themoviedb.org/3/search/tv?api_key=".TMDB_API_KEY."&language=pt-BR&query=".REPLACEMENT_NAME);
        define('TMDB_API_FIND_SERIE_BY_ID',  "https://api.themoviedb.org/3/tv/".REPLACEMENT_ID."?api_key=".TMDB_API_KEY."&language=pt-BR");

        //SEASON
        define('TMDB_API_FIND_SEASON_BY_SERIE',  "https://api.themoviedb.org/3/tv/".REPLACEMENT_ID."/season/".REPLACEMENT_SEASON."?api_key=".TMDB_API_KEY."&language=pt-BR");

        define('TMDB_API_FIND_EPISODE_BY_SERIE',  "https://api.themoviedb.org/3/tv/".REPLACEMENT_ID."/season/".REPLACEMENT_SEASON."/episode/".REPLACEMENT_EPISODE."?api_key=".TMDB_API_KEY."&language=pt-BR");

        define('PATH_RESOURCE_SERIES', '/public/series/');
        define('PATH_RESOURCE_MOVIES', '/public/movies/');
        define('PATH_RESOURCE_STREAMS', '/public/streams/');

        define('URL_RESOURCE_ORIRINAL', "http://image.tmdb.org/t/p/original");
    }
}
