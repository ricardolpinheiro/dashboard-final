<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TvSerie extends Base
{
    public static $cols = ["uuid", 'title', "original_name",'backdrop_path', 'poster_path'];

    protected $fillable = [
        "title",
        "original_name",
        "tmdb_id",
        "category_id",
        "poster_path",
        "backdrop_path",
        "detail",
        "rating",
        "type",
    ];

    public function bouquets() {
        return $this->hasMany(BouquetRelation::class, 'resource_id', 'uuid');
    }

    public function available_bouquets() {
        return $this->bouquets()->where('resource', '=', 'serie');
    }

    public function seasons() {
        return $this->hasMany(TvSerieSeason::class, 'tv_series_id', 'uuid');
    }
}
