@extends('layouts.app')

@section('content')
        <div class="element-wrapper">
            <div class="col-md-12">
               <div class="row">
                   <div class="col-md-6">
                       <h6 class="element-header">
                           Gestão de {{getTitlePage($res)}}
                       </h6>
                   </div>
                   <div class="col-md-6 text-right">
                       <a href="{{ route('admins.create') }}" class="mr-2 mb-2 btn btn-primary btn-md" type="button">Cadastrar Usuário</a>
                   </div>
               </div>
            </div>

            <div class="element-box">
                <x-message :message="Session::has('success') ? Session::get('success') : ''"/>

                <div class="table-responsive">
                    {{ csrf_field() }}
                    <table id="users-table" class="table table-striped table-lightfont">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Username</th>
                                <th width="25%">Actions</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
@endsection

@section('scripts')
    @include('admin.admins.js.password')
    @include('admin.common.delete')
@endsection
