@extends('layouts.app')

@section('content')
    <div class="element-wrapper">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <h6 class="element-header">
                        Edição de {{ getTitlePage($res) }}
                    </h6>
                </div>
                <div class="col-md-6 text-right">
                    <button onclick="window.history.back();"  class="mr-2 mb-2 btn btn-primary btn-md" type="button">Voltar</button>
                </div>
            </div>
        </div>
        {!! Form::model($model, ['route' => [$res.'.update', $model->uuid], 'method' => 'patch']) !!}
            <div class="row">
                <div class="col-md-6">
                    <div class="element-box">

                        @include('admin.'.$res.'.fields')

                        <div class="form-group col-sm-12">
                            {!! Form::submit('Salvar', ['class' => 'btn btn-primary']) !!}
                        </div>

                    </div>

                </div>
                <div class="col-md-6">
                    <div class="element-box">
                        <div class="com-md-12">
                            <div id="btnLink">
                                @foreach($model->links as $key => $link)
                                    @if($key === 0)
                                        <div class="input-group mb-3">
                                                <input type="text" name="link[]" value="{{$link->url}}" class="form-control" placeholder="https://" aria-label="" aria-describedby="basic-addon1">
                                            <div class="input-group-prepend">
                                                <button class="btn btn-outline-secondary" onclick="addField()" type="button">
                                                    <i class="os-icon os-icon-ui-22"></i>
                                                </button>
                                            </div>
                                        </div>
                                    @else
                                        <div class="input-group mb-3">
                                                <input type="text" name="link[]" value="{{$link->url}}" class="form-control" placeholder="https://" aria-label="" aria-describedby="basic-addon1">'

                                            <div class="input-group-prepend">
                                                <button class="btn btn-outline-danger" onclick="removeItem(this)" type="button"><i class="os-icon os-icon-x-circle"></i></button>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        {!! Form::close() !!}

    </div>

@endsection

@section('scripts')
    <script>
        function addField() {
            var field = ' <div class="input-group mb-3">' +

                '<input type="text" name="link[]" class="form-control" placeholder="https://" aria-label="" aria-describedby="basic-addon1">' +

                '<div class="input-group-prepend">' +
                '<button class="btn btn-outline-danger" onclick="removeItem(this)" type="button"><i class="os-icon os-icon-x-circle"></i></button>' +
                '</div>' +
                '</div>';

            $("#btnLink").append(field);
        }

        function removeItem(el) {
            $(el).parent().parent().remove();
        }
    </script>
@endsection
