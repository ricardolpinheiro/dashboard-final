if($('#users-table')) {
    $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '/datatable/admins',
            data: {
                csrf_token: $('meta[name=_token]').attr("content")
            }
        },
        columns: [
            {data: 'name', name: 'name'},
            {data: 'username', name: 'username'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });
}

if($('#revenda-table')) {
    $('#revenda-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '/datatable/revendas',
            data: {
                csrf_token: $('meta[name=_token]').attr("content")
            }
        },
        columns: [
            {data: 'name', name: 'name'},
            {data: 'username', name: 'username'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });
}

if($('#bouquet-table')) {
    $('#bouquet-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '/datatable/bouquets',
            data: {
                csrf_token: $('meta[name=_token]').attr("content")
            }
        },
        columns: [
            {data: 'name', name: 'name'},
            {data: 'status', name: 'status'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });
}

if($('#categories-table')) {
    $('#categories-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '/datatable/categories',
            data: {
                csrf_token: $('meta[name=_token]').attr("content")
            }
        },
        columns: [
            {data: 'name', name: 'name'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });
}

if($('#hosts-table')) {
    $('#hosts-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '/datatable/hosts',
            data: {
                csrf_token: $('meta[name=_token]').attr("content")
            }
        },
        columns: [
            {data: 'host_name', name: 'host_name'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });
}


if($('#streams-table')) {
    $('#streams-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '/datatable/streams',
            data: {
                csrf_token: $('meta[name=_token]').attr("content")
            }
        },
        columns: [
            {data: 'name', name: 'name'},
            {data: 'status', name: 'status'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });
}

if($('#movies-table')) {
    $('#movies-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '/datatable/movies',
            data: {
                csrf_token: $('meta[name=_token]').attr("content")
            }
        },
        columns: [
            {data: 'title', name: 'title'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });
}


if($('#radios-table')) {
    $('#radios-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '/datatable/radios',
            data: {
                csrf_token: $('meta[name=_token]').attr("content")
            }
        },
        columns: [
            {data: 'name', name: 'name'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });
}

if($('#series-table')) {
    $('#series-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '/datatable/series',
            data: {
                csrf_token: $('meta[name=_token]').attr("content")
            }
        },
        columns: [
            {data: 'title', name: 'title'},
            {data: 'original_name', name: 'original_name'},
            {data: 'poster', name: 'poster'},
            {data: 'background', name: 'background'},
            {data: 'bouquets', name: 'bouquets'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });
}

if($('#seasons-table')) {
    $('#seasons-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '/datatable/seasons',
            data: {
                csrf_token: $('meta[name=_token]').attr("content")
            }
        },
        columns: [
            {data: 'serie', name: 'serie'},
            {data: 'season_no', name: 'season_no'},
            {data: 'poster', name: 'poster'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });
}

if($('#episodes-table')) {
    $('#episodes-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '/datatable/episodes',
            data: {
                csrf_token: $('meta[name=_token]').attr("content")
            }
        },
        columns: [
            {data: 'serie', name: 'serie'},
            {data: 'season_no', name: 'season_no'},
            {data: 'episode_no', name: 'episode_no'},
            {data: 'thumbnail', name: 'thumbnail'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });
}
