<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class TvSerieEpisode extends Base
{
    public static $cols = ["uuid", "seasons_id", "episode_no", "thumbnail"];

    protected $fillable = [
        "seasons_id",
        "episode_no",
        "title",
        "category_id",
        "episode_link",
        "thumbnail",
        "detail",
    ];

    public function season() {
        return $this->hasOne(TvSerieSeason::class, 'uuid', 'seasons_id');
    }
}
