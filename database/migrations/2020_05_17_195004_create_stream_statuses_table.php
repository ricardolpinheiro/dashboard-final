<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStreamStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stream_statuses', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->nullable();
            $table->uuid('stream_id')->nullable();
            $table->uuid('link_id')->nullable();
            $table->integer('uptime')->nullable();
            $table->integer('donwtime')->nullable();
            $table->string('status')->nullable();
            $table->timestamp('last_check_uptime')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stream_statuses');
    }
}
