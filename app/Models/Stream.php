<?php

namespace App\Models;

use App\Models\BouquetRelation;

use App\Models\c;
use Illuminate\Database\Eloquent\Model;

class Stream extends Base
{
    protected $fillable = [
        "uuid",
        "name",
        "category_id",
        "stream_logo",
        "epg_id",
        "host_id",
        "notes",
        "status",
    ];

    public static $cols = [ "uuid", "name", "stream_logo", "status"];

    public function bouquets() {
        return $this->hasMany(BouquetRelation::class, 'resource_id', 'uuid');
    }

//    public function epg() {
//        return $this->belongsTo(EpgChannel::class, 'epg_id');
//    }

    public function available_bouquets() {
        return $this->bouquets()->where('resource', '=', 'stream');
    }

    public function host() {
        return $this->belongsTo(HostStream::class);
    }

    public function links() {
        return $this->hasMany(StreamLink::class, 'stream_id', 'uuid');
    }

    public function check() {
        return $this->hasMany(StreamStatus::class, 'stream_id', 'uuid');
    }
}
