<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Bouquet;
use App\Models\BouquetRelation;
use App\Models\CategoryStream;
use App\Models\Movie as Resource;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class MovieController extends Controller
{
    private $res = 'movies';

    private $categories;
    private $bouquets;
    private $epgs;

    public function __construct()
    {
        $categories = CategoryStream::all();
        $categories = $categories->toArray();

        $bouquets = Bouquet::where('status', 1)->get();
        $bouquets = $bouquets->toArray();

//        $epgs = EpgChannel::all();
//        $epgs = $epgs->toArray();


        $this->categories =  parseSelectArray($categories, 'name');
        $this->bouquets =  parseSelectArray($bouquets, 'name');
//        $this->epgs =  parseSelectArray($epgs, 'nome', true, 'id_canal');;
        $this->epgs =  [];
    }

    public function index()
    {
        $res = $this->res;
        return view('admin.'.$this->res.'.index')->with(compact('res'));
    }

    public function datatable(Request $request) {

        $model = Resource::select(Resource::$cols)->get();
        return Datatables::of($model)
            ->addColumn('action', function ($model) {
                $btn = btnTableEdit($this->res.'.edit', $model);
                //$btn .= ($model->status === 0) ? btnTableActive($model) : btnTableInactive($model);
                $btn .= btnTableDelete($this->res.'.destroy', $model);
                return $btn;
            })
            ->editColumn('status', function ($model) {
                return bedgeTableStatus($model->status);
            })
            ->rawColumns(['status', 'action'])
            ->make(true);
    }

    public function create()
    {
        $res = $this->res;
        $categories = $this->categories;
        $bouquets = $this->bouquets;
        $epgs = $this->epgs;
        return view('admin.'.$this->res.'.create')->with(compact('res','categories', 'bouquets', 'epgs'));
    }

    public function store(Request $request)
    {
        $res = $this->res;

        $request->validate([
            'title' => 'required_without:tmdb_id',
            'category_id' => 'required|exists:category_streams,uuid',
            'movie_link' => 'required|url',
        ]);

        $input = $request->all();
        $movie = Resource::create($input);

        if($movie && isset($input['bouquet_id'])) {
            foreach ($input['bouquet_id'] as $bouquet){
                BouquetRelation::create(['resource_id' => $movie->uuid, 'resource' => 'movie', 'bouquet_id'=>$bouquet]);
            }
        }

        return redirect(route($this->res.'.index'))->with('success', __('messages.'.$this->res.'.created'));
    }

    public function show($id)
    {
        $res = $this->res;
        $model = Resource::where('uuid', '=', $id)->first();

        if (empty($model)) {
            return redirect(route($this->res.'.index'))->with('error', __('messages.'.$this->res.'.not_found'));
        }

        return view('admin.'.$this->res.'.show')->with(compact('model'));
    }

    public function edit($id)
    {
        $res = $this->res;
        $categories = $this->categories;
        $bouquets = $this->bouquets;
        $epgs = $this->epgs;
        $model = Resource::where('uuid', '=', $id)->first();

        if (empty($model)) {
            return redirect(route($this->res.'.index'))->with('error', __('messages.'.$this->res.'.not_found'));
        }

        return view('admin.'.$this->res.'.edit')->with(compact('model', 'res','categories', 'bouquets', 'epgs'));
    }

    public function update($id, Request $request)
    {
        $input = $request->all();
        $model = Resource::where('uuid', '=', $id)->first();
        $reload = false;

        if (empty($model))
            return redirect(route($this->res.'.index'))->with('error', __('messages.'.$this->res.'.not_found'));

        if($input['tmdb_id'] !== $model->tmdb_id || $input['tmdb_id'] == '')
            $reload = true;

        $validate = [
            'category_id' => 'exists:category_streams,uuid',
            'movie_link' => 'url',
            'trailer_url' => 'url',
            'thumbnail' => 'mimes:jpeg,bmp,png,jpg',
            'poster' => 'mimes:jpeg,bmp,png,jpg',
        ];

        if(empty($request->get('thumbnail')))
            unset($validate['thumbnail']);

        if(empty($request->get('poster')))
            unset($validate['poster']);

        if(empty($request->get('trailer_url'))) {
            unset($input['trailer_url']);
            unset($validate['trailer_url']);
        }

        $request->validate($validate);

        if(empty($request->get('thumbnail')))
            unset($input['thumbnail']);

        if($request->hasFile('thumbnail')){
            $input['thumbnail'] = storage($request, 'thumbnail', '/img/movie/');
        } else {
            unset($input['thumbnail']);
        }

        if($request->hasFile('poster')){
            $input['poster'] = storage($request, 'poster', '/img/movie/');
        } else {
            unset($input['poster']);
        }


        $model->fill($input);
        $model->save();

        if($model){
            $relation = BouquetRelation::where(['resource_id' => $model->uuid, 'resource' => 'movie']);
            if($relation->count()) {
                $relation->delete();
            }
            if(isset($input['bouquet_id'])){
                foreach ($input['bouquet_id'] as $bouquet){
                    BouquetRelation::create(['resource_id' => $model->uuid, 'resource' => 'movie', 'bouquet_id'=>$bouquet]);
                }
            }

        }

        //if($reload) dispatch(new SyncTMDBMovie($movie));

        return redirect(route($this->res.'.index'))->with('success', __('messages.'.$this->res.'.update'));
    }


    public function destroy($id)
    {
        $models = Resource::where('uuid', '=', $id)->first();

        if (empty($models))
            return redirect(route($this->res.'.index'))->with('error', __('messages.'.$this->res.'.not_found'));

        $models->delete();

        return redirect(route($this->res.'.index'));
    }
}
