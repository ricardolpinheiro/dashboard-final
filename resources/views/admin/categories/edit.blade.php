@extends('layouts.app')

@section('content')
    <div class="element-wrapper">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <h6 class="element-header">
                        Edição de {{ getTitlePage($res) }}
                    </h6>
                </div>
                <div class="col-md-6 text-right">
                    <button onclick="window.history.back();"  class="mr-2 mb-2 btn btn-primary btn-md" type="button">Voltar</button>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="element-box">
                {!! Form::model($model, ['route' => [$res.'.update', $model->uuid], 'method' => 'patch']) !!}

                @include('admin.'.$res.'.fields')

                <div class="form-group col-sm-12">
                    {!! Form::submit('Salvar', ['class' => 'btn btn-primary']) !!}
                </div>

                {!! Form::close() !!}
            </div>

        </div>
    </div>

@endsection
