<?php

namespace App\Console\Commands;

use App\Models\Stream;
use App\Models\StreamStatus;
use App\Process;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class CheckStream extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:stream';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $streams = Stream::all();

        foreach ($streams as $stream) {
            foreach ($stream->links() as $link) {
                $date = now();

                if(!$stream->check()->count()){
                    $status = new StreamStatus();
                    $status->link_id = $link->uuid;
                    $status->stream_id = $stream->uuid;
                    $status->save();
                } else {
                    $status = $stream->check()->first();
                }

                $last = $status->last_check_uptime ?? now();

//                $process = new Process(["C:\\ffmpeg\\bin\\ffprobe", "-v", "quiet", "-print_format", "json", "-show_streams", $link->url]);
//                $process->mustRun();
//                $output = $process->getOutput();
                $process = new Process("C:\\ffmpeg\\bin\\ffprobe -v quiet -print_format json -show_streams" . $link->url);


                $process->start();
                dd($process->status());
//                if ($process.status()){
//                    echo "The process is currently running";
//                }else{
//                    echo "The process is not running.";
//                }

                Log::info($output);
                $status->last_check_uptime = $date;

                if(is_array($output)){
                    $status->status = 'up';
                    $status->uptime += $last->diffInMinutes($date);
                    $status->donwtime = 0;
                } else {
                    $status->status = 'down';
                    $status->uptime = 0;
                    $status->donwtime += $last->diffInMinutes($date);
                }

                $status->save();
            }
        }

        return;
    }
}

