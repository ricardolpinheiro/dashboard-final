<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Movie;
use App\Models\Stream;
use App\Models\TvSerie;
use Illuminate\Http\Request;

class ServiceController extends Controller
{

    public function login(){}

    public function fetch() {
        $revenda = [''];
        $bouquets_revenda = [''];
        $movies = [''];
        $series = [''];
        $streams = [];
        $radios = [];
        $home_banner = [];
        $home_stream = [];

        $movies = Movie::all();
        $series = TvSerie::with('seasons.episodes')->get();
        $streams = Stream::all();


        return [
            'data'=> [
                'revenda'=> ['name' => 'Teste Revenda'],
                'home_banner' => [],
                'home_stream' => [''],
                'stream' => $streams,
                'movie' => $movies,
                'serie' => $series
            ]
        ];
    }

}
